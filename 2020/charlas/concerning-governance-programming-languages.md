# Concerning Governance in Programming Languages

En esta charla realizaré un análisis crítico de los aspectos más importantes de la gobernanza de Lenguajes de Programación.

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

A menudo me he preguntado por qué siempre nos centramos en los detalles técnicos, olvidando los **aspectos éticos** de la tecnología. Hace unos años decidí centrarme en analizar el asunto en lo relativo a los lenguajes de programación, pero el enfoque se puede aplicar a otros conceptos. Esta charla no trata sobre qué lenguaje de programación tiene esta o aquella capacidad, o cuál tiene más rasgos funcionales. Lo que quería analizar es **cómo de ético, diverso y saludable es un lenguaje de programación y la comunidad que lo rodea**.

En esta charla explico lo que se puede medir y analizar con respecto a la **ética** y la **gobernanza** en los lenguajes de programación, y presentaré un **análisis crítico** de algunos de los principales lenguajes, no solo por ser más populares sino especialmente aquellos con alguna peculiaridad especial.

¿Qué puntos fuertes o débiles tiene la gobernanza de cada uno? ¿Quién marca los objetivos? ¿Quién prioriza? ¿Quién colabora en su desarrollo?

En definitiva, **¿Quién está al mando de tu lenguaje de programación favorito?**

## Público objetivo

A todo el mundo, no es necesario un nivel o conocimiento previo. Especialmente recomendada a aquellos interesados en los aspectos éticos del FOSS.

## Ponente(s)

Luis García Castro.

**Arquitecto de Software** y **FOSS advocate**, me encanta aprender y enseñar, por lo que procuro dar tantas charlas y formaciones como pueda. Soy miembro entre otras asociaciones de la **Free Software Foundation Europe** y de la **OpenStreetMap Foundation**. Por mis contribuciones, también tengo la categoría de  _super mapper_ en OpenStreetMap.

Aparte de mi activismo, lidero el área de Information Architecture en Maggie, un programa internacional de **ING** diseñando desde cero la que será la mejor plataforma bancaria multi-país.

Mis principales charlas públicas son: https://luiyo.net/talks/

### Contacto(s)

* Luis García Castro: luisgc@gmail.com

## Comentarios

Ya impartí una versión más larga de esta charla en Commit Conf 2019, y a su vez una versión anterior de la misma hace 5 años. Para 25 minutos centraré la charla en los principales aspectos y limitaré el número de ejemplos a los más representativos.

La charla podría ser en inglés, aunque la propongo en castellano.

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
