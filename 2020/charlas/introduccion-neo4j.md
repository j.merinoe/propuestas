# Introducción a bases de datos de grafos con Neo4j

Introducción a bases de datos de grafos con ejemplos en vivo usando [Neo4J](https://neo4j.com/)

## Formato de la propuesta

Indicar uno de estos:

* [x] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

Se realizará una introducción al funcionamiento de las bases de datos de grafos como alternativa a las bases de datos tradicionales (SQL, documentales...), con ejemplos en vivo usando [Neo4J](https://neo4j.com/), una base de datos [open source](https://github.com/neo4j/neo4j).

Los asistentes podrán tener instalado la herramienta de escritorio para poder probar los ejemplos en sus propios sistemas. (https://neo4j.com/download/)

## Público objetivo
No es necesario ningun conocmiento, al ser una introducción, pero se asumirá algun conocimiento básico en otras bases de datos tradicionales.

## Ponente(s)

Andrés Ortiz <angrykoala>

### Contacto(s)

* Nombre: Andrés Ortiz
* Contacto: andresortizcorrales@gmail.com

## Condiciones

* [x] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [x] Al menos una persona entre los que la proponen estará presente el día programado para la charla.