# Web scrapeando las tecnologías más usadas de la Web

¿Qué tecnologías son las más usadas en la Web? ¿Son ciertos los estudios que se publican?  Mediante Web Scraping Python se puede obtener esta información y mucho más

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

Partiendo de dos fuentes de información: el millón de sitios más usados según alexa y todos los sitios web registrados en españa descargados desde red.es de ha realizado un web scraping sobre 
esas URL's y se he pasado un analizador, wappalizer, y he extraído todas las tecnologías usadas en la web detectables con este detector. Entre ellas se destacan: lenguajes de programación, 
bases de datos, servidores web,frameworks de frontend, backend, CSS, CMS's, plugins de CMS.
Con esos datos metidos en un mongo, se han generado las gráficas por conjuntos de tecnologías web, tanto para el millón de alexa como para todos los dominios en España.
La idea de la charla es dar a conocer estos datos actualizados, que pueden ser de interés para mucha gente y contar cómo lo he hecho.
En esta charla mostraremos los resultados de un estudio de las tecnologías más usadas en la web tanto en el mundo como en España y explicaremos cómo se ha hecho el estudio con Python, 
Wappalizer y MongoDB.

## Público objetivo

Toda aquella persona interesada en el uso de las tecnologías, las empresas de formación, y sobre todo aquellas que diseñan planes formativos en las empresas TIC.

## Ponente(s)

David Vaquero Santiago (Pepesan)
https://www.linkedin.com/in/davidvaquero/
https://cursosdedesarrollo.com/
https://republicaweb.es/

### Contacto(s)

* Nombre: David Vaquero Santiago
* Correo: pepesan@gmail.com
* GitLab: @pepesan


## Comentarios

En previsión a lo que pueda ocurrir con la agenda, la charla debería ser en Sábado, porque lo más probable es que el viernes esté trabajando

## Condiciones

* [ X] Acepto seguir el [código de conducta](https://eslib.re/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [ X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
