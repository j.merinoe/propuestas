# Introducción a la Cria de Camellos con Apache Camel

En este taller aprenderemos cómo construir workflows de datos con Apache Camel de forma ágil y desacoplada.


## Objetivos a cubrir en el taller


Apache Camel es un framework de integración de procesos que permite integrar fácilmente varios sistemas que consumen o producen datos. Incluye los más conocidos Enterprise Integration Patterns para poder implementar flujos de trabajo complejos. Apache Camel es uno de los proyectos más activos de la Fundación Apache y es la base de muchos otros proyectos de FLOSS.

Comenzaremos con una simple integración ("hello world") e iremos iterando sobre este ejemplo agregando patrones y conexiones más complejas hasta que se acabe el tiempo.

## Público objetivo

A analistas de datos, desarrolladores y toda persona que trabaje con flujos de datos.

## Ponente(s)

María Arias de Reyna: Senior Software Engineer y evangelizadora del FOSS. Actualmente trabaja en Red Hat, donde se enfoca en Middleware y mantiene Apache Camel y Syndesis. Entre 2017 y 2019 fue Presidenta de OSGeo, la Fundación Geoespacial de Código Abierto. 

### Contacto(s)

* María Arias de Reyna: delawen @ gmail

## Prerequisitos para los asistentes

* No son necesarios conocimientos mínimos, es un taller introductorio. 
* Ordenador/portátil con 
  * editor de textos
  * conexión a internet
  

Intentaremos usar algo tipo dockerlabs para ejecutar el código y no depender de las máquinas locales, pero depende de la conexión a internet de la que dispongamos, así que, por si acaso, los ordenadores deben traer también:

  * maven (https://maven.apache.org/)


## Prerequisitos para la organización

 * Proyector con pantalla
 * Internet estable


## Tiempo

2 horas

## Día

Preferible primer día (viernes)

## Comentarios


## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/conducta/).
* [X] Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [X] Acepto coordinarme con la organización de esLibre.
